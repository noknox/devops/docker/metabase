FROM metabase/metabase

RUN wget https://github.com/dacort/metabase-athena-driver/releases/download/v1.0.0/athena.metabase-driver.jar -O /plugins/athena.metabase-driver.jar
